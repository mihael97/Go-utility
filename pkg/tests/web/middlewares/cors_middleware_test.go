package middlewares

import (
	"gitlab.com/mihael97/Go-utility/pkg/web/middlewares"
	"net/http"
	"net/http/httptest"
	"testing"
)

type testCase struct {
	host        string
	allowedHost string
	invalidCors bool
	skipCors    bool
}

func TestSuccessCors(t *testing.T) {
	for _, testCase := range []testCase{{host: "localhost:3000", allowedHost: "*"}, {host: "localhost:3000", allowedHost: "http://localhost:3000"}} {
		testRequest(t, testCase)
	}
}

func TestInvalidCors(t *testing.T) {
	for _, testCase := range []testCase{{host: "localhost:3000", allowedHost: "http://localhost:3001", invalidCors: true}} {
		testRequest(t, testCase)
	}
}

func TestSkipCors(t *testing.T) {
	for _, testCase := range []testCase{{host: "localhost:3000", allowedHost: "http://localhost:3000", invalidCors: true, skipCors: true}} {
		testRequest(t, testCase)
	}
}

func testRequest(t *testing.T, testCase testCase) {
	if testCase.skipCors {
		t.Setenv("CORS_SKIP", "true")
	}
	t.Setenv("CORS_ALLOWED_ORIGIN", testCase.allowedHost)
	r := createRouter()
	origin := "http://" + testCase.host

	server := httptest.NewServer(r)
	defer server.Close()

	client := &http.Client{}
	req, _ := http.NewRequest(
		"GET",
		"http://"+server.Listener.Addr().String()+"/api",
		nil,
	)
	req.Header.Add("Origin", origin)
	req.Header.Add("Host", testCase.host)

	get, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	o := get.Header.Get("Access-Control-Allow-Origin")

	if testCase.invalidCors {
		var expectedStatus int
		if testCase.skipCors {
			expectedStatus = http.StatusOK
		} else {
			expectedStatus = http.StatusForbidden
		}
		if get.StatusCode != expectedStatus {
			t.Errorf("Expected status %d, got %d", http.StatusForbidden, get.StatusCode)
		}
	} else {
		if o != testCase.allowedHost {
			t.Errorf("Got '%s' ; expecting origin '%s'", o, testCase.allowedHost)
		}
	}
}

func test(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte("test"))
}

func createRouter() *http.ServeMux {
	r := http.NewServeMux()
	r.Handle("GET /api", middlewares.CORSMiddleware()(http.HandlerFunc(test)))
	return r
}
