package mapper

import (
	"gitlab.com/mihael97/Go-utility/pkg/mapper"
	"testing"
)

type structA struct {
	Id string
}

type structB struct {
	Id string
}

func TestMapCorrect(t *testing.T) {
	a := structA{Id: "1"}

	modelMapper := mapper.GetModelDtoMapper[structA, structB]()

	mapped := modelMapper.MapItem(a)

	if mapped.Id != a.Id {
		t.Error("Fields are not the same")
	}
}
