package mapper

import (
	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/mihael97/Go-utility/pkg/database/mapper"
	"log"
	"testing"
)

type testStruct struct {
	Id         string
	Name       string
	IntValue   int
	FloatValue float64
}

func TestOne(t *testing.T) {
	db, mock, _ := sqlmock.New()
	rows := sqlmock.NewRows([]string{"id", "name", "int_value"}).AddRow("1", nil, 2).AddRow("2", "Hehe", 3)
	mock.ExpectBegin()
	mock.ExpectQuery("SELECT").WillReturnRows(rows)
	mock.ExpectCommit()

	tx, _ := db.Begin()
	queryRows, err := tx.Query("SELECT")
	if err != nil {
		panic(err)
	}

	databaseMapper := mapper.GetDatabaseMapper(func(item mapper.SqlRowsData) testStruct {
		test := testStruct{}
		test.Id = item.GetString("id")
		if item.HasValue("name") {
			test.Name = item.GetString("name")
		}
		test.IntValue = item.GetInt("int_value")
		return test
	})

	expectedResults := []testStruct{
		{Id: "1", Name: "", IntValue: 2},
		{Id: "2", Name: "Hehe", IntValue: 3},
	}

	actualResults, err := databaseMapper.MapItems(queryRows)
	if err != nil {
		t.Error(err)
	} else {
		if actualResults == nil {
			t.Errorf("Item is null")
		} else {
			for i := range expectedResults {
				log.Printf("Row %d\n", i)
				actualResult := actualResults[i]
				expectedResult := expectedResults[i]
				if actualResult.Id != expectedResult.Id {
					t.Errorf("Id should be %s but is %s", expectedResult.Id, actualResult.Id)
				}
				if actualResult.Name != expectedResult.Name {
					t.Errorf("Name should be %s but is %s", expectedResult.Name, actualResult.Name)
				}
				if actualResult.IntValue != expectedResult.IntValue {
					t.Errorf("IntValue should be %d but is %d", expectedResult.IntValue, actualResult.IntValue)
				}
			}
		}
	}
}

func TestTwo(t *testing.T) {
	db, mock, _ := sqlmock.New()
	rows := sqlmock.NewRows([]string{"float_value"}).AddRow("1.2").AddRow(1.4)
	mock.ExpectBegin()
	mock.ExpectQuery("SELECT").WillReturnRows(rows)
	mock.ExpectCommit()

	tx, _ := db.Begin()
	queryRows, err := tx.Query("SELECT")
	if err != nil {
		panic(err)
	}

	databaseMapper := mapper.GetDatabaseMapper(func(item mapper.SqlRowsData) testStruct {
		test := testStruct{}
		test.FloatValue = item.GetFloat64("float_value")
		return test
	})

	expectedResults := []testStruct{
		{FloatValue: 1.2},
		{FloatValue: 1.4},
	}

	actualResults, err := databaseMapper.MapItems(queryRows)
	if err != nil {
		t.Error(err)
	} else {
		if actualResults == nil {
			t.Errorf("Item is null")
		} else {
			for i := range expectedResults {
				log.Printf("Row %d\n", i)
				actualResult := actualResults[i]
				expectedResult := expectedResults[i]
				if actualResult.FloatValue != expectedResult.FloatValue {
					t.Errorf("Float_value should be %f but is %f", expectedResult.FloatValue, actualResult.FloatValue)
				}
			}
		}
	}
}
