package util

import (
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"testing"
)

func TestSingletonList(t *testing.T) {
	item := util.Exception{Type: "a"}

	result := util.SingletonList(item)

	if len(result) != 1 {
		t.Errorf("Error should be one but is %d", len(result))
	} else {
		if result[0].Type != "a" {
			t.Errorf("Type should be 'a' but is '%s'", result[0].Type)
		}
	}
}
