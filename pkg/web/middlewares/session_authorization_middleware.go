package middlewares

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/mihael97/Go-utility/pkg/web/security/session"
	"net/http"
)

func GetSessionAuthorizationMiddleware(nextHandler http.Handler) http.Handler {
	maker := session.GetSessionMaker()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := maker.VerifyToken(r)
		if err != nil {
			logrus.Info(err.Error())
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		nextHandler.ServeHTTP(w, r)
	})
}
