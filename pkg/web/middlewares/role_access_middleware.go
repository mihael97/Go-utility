package middlewares

import (
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"gitlab.com/mihael97/Go-utility/pkg/web"
	"gitlab.com/mihael97/Go-utility/pkg/web/security/jwt"
	"log"
	"net/http"
)

// RolesMiddleware checks if user has sufficient roles to access endpoint
func RolesMiddleware(securedRoutes []string, authFunction AuthFunction) func(http.Handler) http.Handler {
	authHeaderName := env.GetEnvVariable(jwt.AuthorizationHeaderEnvKey, jwt.DefaultAuthorizationHeaderName)

	return func(nextHandler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if util.Contains(web.GetFullPath(r), securedRoutes...) {
				authHeader := r.Header.Get(authHeaderName)
				if len(authHeader) == 0 {
					w.WriteHeader(http.StatusUnauthorized)
				} else if authFunction(r) {
					log.Printf("Token %s is authorized\n", authHeader)
				} else {
					w.WriteHeader(http.StatusForbidden)
					return
				}
			}
			nextHandler.ServeHTTP(w, r)
		})
	}
}

// DefaultRolesMiddleware checks if user has sufficient roles to access endpoint in request header param
func DefaultRolesMiddleware(rolesRoutes map[string][]string) func(http.Handler) http.Handler {
	securedRoutes := util.Keys(rolesRoutes)
	return RolesMiddleware(securedRoutes, GetHeaderAuthFunction(rolesRoutes))
}
