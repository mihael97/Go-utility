package middlewares

import (
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"gitlab.com/mihael97/Go-utility/pkg/web/security/jwt"
	"net/http"
	"strings"
)

const ApiPrefix = "/api"

func JwtAuthorizationMiddleware(secret string, checkIfPermitted func(w http.ResponseWriter, r *http.Request) bool) func(http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if checkIfPermitted(w, r) {
				return
			}
			jwt.CheckSecurityToken(w, r, secret, handler)
		})
	}
}

func GetSecuredRoutesJwtAuthorizationMiddleware(securedRoutes map[string][]string, secret ...string) func(http.Handler) http.Handler {
	if len(secret) == 0 {
		secret = make([]string, 1)
		secret[0] = env.GetEnvVariable(jwt.SecretVariable)
	}

	return JwtAuthorizationMiddleware(secret[0], func(w http.ResponseWriter, r *http.Request) bool {
		route := strings.TrimPrefix(r.Host+r.URL.Path, ApiPrefix)
		methods, exist := securedRoutes[route]
		if exist {
			return !util.Contains(r.Method, methods...)
		}
		return true
	})
}

// GetDefaultJwtAuthorizationMiddleware only login is permitted for all
func GetDefaultJwtAuthorizationMiddleware() func(http.Handler) http.Handler {
	secret := env.GetEnvVariable(jwt.SecretVariable)

	return JwtAuthorizationMiddleware(secret, func(w http.ResponseWriter, r *http.Request) bool {
		return strings.HasSuffix(r.Host+r.URL.Path, "/login")
	})
}
