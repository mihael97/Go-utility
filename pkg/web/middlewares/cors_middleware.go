package middlewares

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"net/http"
	"strconv"
	"strings"
)

const DefaultAllowedOrigin string = "*"
const DefaultAllowedMethods string = "POST,HEAD,PATCH,OPTIONS,GET,PUT,DELETE"

func corsHandler(handlerFunction http.Handler, allowedOrigins, allowedMethods string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !checkIfCorsRequest(r) {
			log.Info("Is not CORS request")
		} else {
			if !checkOrigin(r, allowedOrigins) {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			w.Header().Set("Access-Control-Allow-Origin", allowedOrigins)
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, accept, origin, Cache-Control, X-Requested-With")
			w.Header().Set("Access-Control-Allow-Methods", allowedMethods)
		}
		handlerFunction.ServeHTTP(w, r)
	})
}

func CORSMiddleware() func(http.Handler) http.Handler {
	if value, _ := strconv.ParseBool(env.GetEnvVariable("CORS_SKIP", "false")); value {
		log.Info("Skip CORS")
		return func(handler http.Handler) http.Handler {
			return handler
		}
	}

	allowedOrigins := env.GetEnvVariable("CORS_ALLOWED_ORIGIN", DefaultAllowedOrigin)
	allowedMethods := env.GetEnvVariable("CORS_ALLOWED_METHOD", DefaultAllowedMethods)

	log.Infof("Allowed origins: %s", allowedOrigins)
	log.Infof("Allowed methods: %s", allowedMethods)

	return func(handler http.Handler) http.Handler {
		return corsHandler(handler, allowedOrigins, allowedMethods)
	}
}

func checkOrigin(c *http.Request, allowedOrigins string) bool {
	if allowedOrigins == "*" {
		log.Debug("All origins allowed")
		return true
	}
	origins := util.Map(strings.Split(allowedOrigins, ","), func(a string) string {
		return strings.TrimSpace(a)
	})
	return util.Contains(c.Header.Get("Origin"), origins...)
}

func checkIfCorsRequest(c *http.Request) bool {
	origin := c.Header.Get("Origin")
	if len(origin) == 0 {
		log.Info("Origin not present")
		return false
	}

	host := c.Host
	if origin == "http://"+host || origin == "https://"+host {
		log.Info("Origin is not host")
		return false
	}

	return true
}
