package middlewares

import (
	"gitlab.com/mihael97/Go-utility/pkg/web/security/context"
	"net/http"
)

func ContextMiddleware(nextHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		context.StartUserContext(request)
		nextHandler.ServeHTTP(writer, request)
	})
}
