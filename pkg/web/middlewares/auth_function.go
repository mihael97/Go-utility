package middlewares

import (
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"net/http"
	"strings"
)

const RoleHeaderEnvKey = "ROLES_HEADER_KEY"
const RoleHeaderEnvDefault = "X-MACUKA-ROLES"

type AuthFunction func(ctx *http.Request) bool

// GetHeaderAuthFunction checks if user contains any role if endpoint is restricted for some roles
func GetHeaderAuthFunction(routes map[string][]string) func(r *http.Request) bool {
	return func(r *http.Request) bool {
		route := r.Host + r.URL.Path
		value, exists := routes[route]
		if exists {
			rolesHeader := r.Header.Get(env.GetEnvVariable(RoleHeaderEnvKey, RoleHeaderEnvDefault))
			return util.ContainsAny(strings.Split(rolesHeader, ","), value)
		}
		return true
	}
}
