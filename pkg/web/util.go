package web

import "net/http"

type CookieRoundTripper struct {
	Cookies map[string]string `json:"cookies"`
}

func (h *CookieRoundTripper) RoundTrip(r *http.Request) (*http.Response, error) {
	for k, v := range h.Cookies {
		r.AddCookie(&http.Cookie{Name: k, Value: v})
	}
	return http.DefaultTransport.RoundTrip(r)
}

func GetFullPath(r *http.Request) string {
	return r.Host + r.URL.Path
}
