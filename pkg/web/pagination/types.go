package pagination

type PaginationDto struct {
	Page  uint  `json:"page"`
	Total uint  `json:"total"`
	Items []any `json:"items"`
}

type PaginationFilter struct {
	Page     uint `json:"page"`
	PageSize uint `json:"pageSize"`
}
