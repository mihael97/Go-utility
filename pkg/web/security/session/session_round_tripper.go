package session

import (
	"gitlab.com/mihael97/Go-utility/pkg/web"
	"net/http"
)

// GetSessionRoundTripper Add session token in HTTP request
func GetSessionRoundTripper(req *http.Request) http.RoundTripper {
	cookieName := GetSessionTokenName()
	cookie, _ := req.Cookie(cookieName)
	return &web.CookieRoundTripper{Cookies: map[string]string{cookieName: cookie.Value}}
}
