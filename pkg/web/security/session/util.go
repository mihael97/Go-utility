package session

import (
	"gitlab.com/mihael97/Go-utility/pkg/env"
)

func GetSessionTokenName() string {
	return env.GetEnvVariable("SESSION_TOKEN")
}
