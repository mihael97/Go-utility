package jwt

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"gitlab.com/mihael97/Go-utility/pkg/web"
	"gitlab.com/mihael97/Go-utility/pkg/web/security"
	"net/http"
	"regexp"
)

func CheckSecurityToken(resp http.ResponseWriter, r *http.Request, secret string, handler http.Handler) {
	authorizationHeader := r.Header.Get(DefaultAuthorizationHeaderName)

	if len(authorizationHeader) == 0 {
		web.ParseToJson(util.NewException("Authorization header is required"), resp, http.StatusUnauthorized)
		return
	}

	bearerToken := regexp.MustCompile(BearerTokenRegex).Split(authorizationHeader, -1)

	if len(bearerToken) != 2 {
		web.ParseToJson(util.NewException("Authorization header is required"), resp, http.StatusUnauthorized)
		return
	}

	token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("there was an error")
		}
		return []byte(secret), nil
	})

	if err != nil {
		web.ParseToJson(util.NewException(err.Error()), resp, http.StatusUnauthorized)
		return
	}

	keyFunc := func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, fmt.Errorf("there was an error")
		}
		return []byte(secret), nil
	}

	jwtToken, err := jwt.ParseWithClaims(bearerToken[1], &security.Payload{}, keyFunc)
	if err != nil {
		web.ParseToJson(util.NewException(err.Error()), resp, http.StatusUnauthorized)
		return
	}
	payload, ok := jwtToken.Claims.(*security.Payload)

	if err := payload.Valid(); err != nil {
		web.ParseToJson(util.NewException("Token expired"), resp, http.StatusUnauthorized)
		return
	} else if !ok {
		web.ParseToJson(util.NewException("Token not ok"), resp, http.StatusUnauthorized)
		return
	}

	if !token.Valid {
		web.ParseToJson(util.NewException("Invalid authorization token"), resp, http.StatusUnauthorized)
		return
	}
	handler.ServeHTTP(resp, r)
}
