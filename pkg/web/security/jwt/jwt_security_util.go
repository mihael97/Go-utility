package jwt

import (
	"net/http"
)

type MethodType string

// GetUserNameFromToken extracts username from user token
func GetUserNameFromToken(r *http.Request, secret string) (string, error) {
	maker, err := NewJwtMaker(secret)
	if err != nil {
		return "", err
	}
	payload, err := maker.VerifyToken(r)
	if err != nil {
		return "", err
	}
	return payload.Username, nil
}
