package routes

import (
	"fmt"
	"github.com/sirupsen/logrus"
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"gitlab.com/mihael97/Go-utility/pkg/web"
	"gitlab.com/mihael97/Go-utility/pkg/web/middlewares"
	"net/http"
	"path"
	"path/filepath"
)

func AddRoutes(routerGroup *http.ServeMux, basePath string, routes map[Route]func(w http.ResponseWriter, r *http.Request), items []func(handler http.Handler) http.Handler) {
	for routeName, route := range routes {
		url := path.Join(basePath, routeName.URL)

		var s http.Handler = http.HandlerFunc(route)
		for _, middleware := range items {
			s = middleware(s)
		}

		if routeName.Type != web.ALL {
			routerGroup.Handle(fmt.Sprintf("%s %s", routeName.Type.String(), url), s)
			logrus.Infof("%s %s", routeName.Type.String(), url)
		} else {
			routerGroup.Handle(url, s)
			logrus.Infof("ALL %s", url)
		}
	}
}

func GetSecuredRoutes(controllers []RoutesController) (routes map[string][]string) {
	routes = make(map[string][]string, 0)

	for _, controller := range controllers {
		for route := range controller.GetRoutes() {
			if route.Secured {
				path := fmt.Sprintf("%s%s", controller.GetBasePath(), route.URL)
				securedMethods, exist := routes[path]
				if !exist {
					securedMethods = make([]string, 0)
				}
				if route.Type == web.ALL {
					allMethods := []web.MethodType{web.GET, web.DELETE, web.PUT, web.POST}
					for _, method := range allMethods {
						securedMethods = append(securedMethods, method.String())
					}
				} else {
					securedMethods = append(securedMethods, route.Type.String())
				}
				routes[path] = securedMethods
			}
		}
	}

	return
}

func AddControllerRoutesWithFilter(addSwagger bool, securityFilter *func(http.Handler) http.Handler, engine *http.ServeMux, controllers []RoutesController, items []func(handler http.Handler) http.Handler, exposeRoutes bool, apiPath ...string) {
	if len(apiPath) == 0 {
		apiPath = []string{"/api"}
	}

	if addSwagger {
		url := filepath.Join(apiPath[0], "/swagger/")
		logrus.Infof("ALL %s", url)
		engine.HandleFunc(url, httpSwagger.Handler())
	}

	securedRoutes := GetSecuredRoutes(controllers)

	for _, controller := range controllers {
		AddRoutes(engine, path.Join(apiPath[0], controller.GetBasePath()), controller.GetRoutes(), items)
	}

	if len(securedRoutes) != 0 && securityFilter != nil {
		(*securityFilter)(engine)
	}

	// expose routes
	if exposeRoutes {
		engine.HandleFunc("GET /routes", func(w http.ResponseWriter, r *http.Request) {
			exposedRoutes := make(map[string][]Route, len(controllers))

			for _, controller := range controllers {
				routes := make([]Route, len(controller.GetRoutes()))

				routeIndex := 0
				for key := range controller.GetRoutes() {
					routes[routeIndex] = key
					routeIndex += 1
				}

				exposedRoutes[controller.GetBasePath()] = routes
			}

			web.ParseToJson(exposedRoutes, w, http.StatusOK)
		})
		logrus.Info("GET /routes")
	}
}

func AddControllerRoutes(engine *http.ServeMux, controllers []RoutesController, apiPath ...string) {
	AddControllerRoutesWithFilter(false, util.GetPointer(func(handler http.Handler) http.Handler {
		return middlewares.GetSessionAuthorizationMiddleware(handler)
	}), engine, controllers, nil, true, apiPath...)
}

func CreateRouter(controllers []RoutesController, middlewaresItems []func(handler http.Handler) http.Handler, apiPath ...string) http.Handler {
	router := http.NewServeMux()

	AddControllerRoutesWithFilter(true, nil, router, controllers, middlewaresItems, false, apiPath...)

	return router
}
