package routes

import "net/http"

type RoutesController interface {
	GetRoutes() map[Route]func(http.ResponseWriter, *http.Request)
	GetBasePath() string
}
