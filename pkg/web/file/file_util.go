package file

import (
	"fmt"
	"net/http"
	"strings"
)

type ExtensionType string

const PdfExtension ExtensionType = "PDF"
const ExcelExtension ExtensionType = "XLSX"

func CreateDocument(content []byte, ctx http.ResponseWriter, extension ExtensionType, name ...string) {
	fileName := "download"
	if len(name) != 0 {
		fileName = name[0]
	}
	fileName += fmt.Sprintf(".%s", strings.ToLower(string(extension)))
	ctx.Header().Set("Content-Description", "File Transfer")
	ctx.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", fileName))
	ctx.Header().Set("Content-Type", "application/octet-stream")
	ctx.WriteHeader(http.StatusOK)
	_, _ = ctx.Write(content)
}
