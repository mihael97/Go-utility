package mapper

import (
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"unsafe"
)

type modelDtoMapper[T, M any] struct {
}

func (m modelDtoMapper[T, M]) MapItem(row T) *M {
	bar := *(*M)(unsafe.Pointer(&row))
	return &bar
}

func (m modelDtoMapper[T, M]) MapItems(rows []T) []M {
	return util.Map(rows, func(a T) M {
		return *m.MapItem(a)
	})
}

func GetModelDtoMapper[T, M any]() Mapper[T, M] {
	return &modelDtoMapper[T, M]{}
}
