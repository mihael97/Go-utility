package mapper

import (
	"database/sql"
	"fmt"
	"gitlab.com/mihael97/Go-utility/pkg/database/mapper"
	util "gitlab.com/mihael97/Go-utility/pkg/types"
	commonUtil "gitlab.com/mihael97/Go-utility/pkg/util"
	"reflect"
	"time"
	"unicode"
)

type structDatabaseMapper[T interface{}] struct {
}

func (s structDatabaseMapper[T]) getDbValue(item T, index int) string {
	field := reflect.TypeOf(item).Field(index)
	dbFieldName, found := field.Tag.Lookup("dbName")
	if !found {
		fieldName := field.Name
		newDbName := ""
		fieldNameRune := []rune(fieldName)

		for i, x := range fieldNameRune {
			if unicode.IsUpper(x) && i != 0 {
				newDbName += "_"
			}
			newDbName += string(unicode.ToLower(x))
		}

		dbFieldName = newDbName
	}

	return dbFieldName
}

func (s structDatabaseMapper[T]) parseTime(row mapper.SqlRowsData, dbFieldName string) *time.Time {
	date := row.GetData(dbFieldName)
	if date == nil {
		return nil
	}

	var parsedDate time.Time

	switch (*date).(type) {
	case time.Time:
		parsedDate = (*date).(time.Time)
	case int64:
		millis := (*date).(int64)
		parsedDate = time.UnixMilli(millis)
	}

	return &parsedDate
}

func (s structDatabaseMapper[T]) parseMillis(row mapper.SqlRowsData, dbFieldName string) *util.MillisTime {
	date := row.GetData(dbFieldName)
	if date == nil {
		return nil
	}

	var parsedDate *util.MillisTime

	switch (*date).(type) {
	case time.Time:
		parsedDate = util.FromDate((*date).(time.Time))
	case int64:
		millis := (*date).(int64)
		parsedDate = util.FromDate(time.UnixMilli(millis))
	}

	return parsedDate
}

func (s structDatabaseMapper[T]) mapItem(row mapper.SqlRowsData) (*T, error) {
	var item T

	typedItem := reflect.TypeOf(item)

	for i := 0; i < typedItem.NumField(); i++ {
		field := typedItem.Field(i)
		dbFieldName := s.getDbValue(item, i)

		reflectValue := reflect.ValueOf(&item).Elem().FieldByName(field.Name)

		if !row.Contains(dbFieldName) {
			continue
		}

		switch field.Type.String() {
		case "string":
			reflectValue.SetString(row.GetString(dbFieldName))
		case "*string":
			reflectValue.Set(reflect.ValueOf(commonUtil.GetPointer(row.GetString(dbFieldName))))
		case "int":
			reflectValue.SetInt(row.GetInt64(dbFieldName))
		case "*int":
			reflectValue.Set(reflect.ValueOf(commonUtil.GetPointer(int(row.GetInt64(dbFieldName)))))
		case "int64":
			reflectValue.SetInt(row.GetInt64(dbFieldName))
		case "*int64":
			reflectValue.Set(reflect.ValueOf(commonUtil.GetPointer(row.GetInt64(dbFieldName))))
		case "uint":
			reflectValue.Set(reflect.ValueOf(uint(row.GetInt64(dbFieldName))))
		case "*uint":
			reflectValue.Set(reflect.ValueOf(commonUtil.GetPointer(uint(row.GetInt64(dbFieldName)))))
		case "uint64":
			reflectValue.Set(reflect.ValueOf(uint64(row.GetInt64(dbFieldName))))
		case "*uint64":
			reflectValue.Set(reflect.ValueOf(commonUtil.GetPointer(uint64(row.GetInt64(dbFieldName)))))
		case "float64":
			reflectValue.SetFloat(row.GetFloat64(dbFieldName))
		case "*float64":
			reflectValue.Set(reflect.ValueOf(commonUtil.GetPointer(row.GetFloat64(dbFieldName))))
		case "bool":
			reflectValue.SetBool(row.GetBool(dbFieldName))
		case "*bool":
			reflectValue.Set(reflect.ValueOf(commonUtil.GetPointer(row.GetBool(dbFieldName))))
		case "time.Time":
			parsedDate := s.parseTime(row, dbFieldName)
			reflectValue.Set(reflect.ValueOf(*parsedDate))
		case "*time.Time":
			parsedDate := s.parseTime(row, dbFieldName)
			reflectValue.Set(reflect.ValueOf(parsedDate))
		case "util.MillisTime":
			parsedDate := s.parseMillis(row, dbFieldName)
			reflectValue.Set(reflect.ValueOf(*parsedDate))
		case "*util.MillisTime":
			parsedDate := s.parseMillis(row, dbFieldName)
			reflectValue.Set(reflect.ValueOf(parsedDate))
		default:
			var value interface{}

			if row.GetData(dbFieldName) != nil {
				value = *row.GetData(dbFieldName)
			} else {
				value = nil
			}

			if value != nil && reflectValue.Kind() == reflect.Pointer && reflectValue.Type().String() == fmt.Sprintf("*%s", reflect.ValueOf(value).Type().String()) {
				break
			}

			reflectValue.Set(reflect.ValueOf(&value).Convert(reflectValue.Type()))
		}
	}

	return &item, nil
}

func (s structDatabaseMapper[T]) MapItem(rows *sql.Rows) (*T, error) {
	mappedRows, err := mapper.MapRows(rows, true)
	if err != nil {
		return nil, err
	} else if mappedRows.IsEmpty() {
		return nil, nil
	}
	return s.mapItem(mappedRows)
}

func (s structDatabaseMapper[T]) MapItems(rows *sql.Rows) (items []T, err error) {
	mappedRows, err := mapper.MapRows(rows)
	if err != nil {
		return nil, err
	}
	items = make([]T, 0)

	for mappedRows.HasNext() {
		item, err := s.mapItem(mappedRows)
		if err != nil {
			return nil, err
		}
		items = append(items, *item)
	}

	return
}

func GetStructDatabaseMapper[T interface{}]() mapper.DatabaseMapper[T] {
	return structDatabaseMapper[T]{}
}
