package mapper

import "gitlab.com/mihael97/Go-utility/pkg/util"

type defaultMapperImpl[T, M any] struct {
	mapItem func(item T) *M
}

func (d defaultMapperImpl[T, M]) MapItem(row T) *M {
	return d.mapItem(row)
}

func (d defaultMapperImpl[T, M]) MapItems(rows []T) []M {
	return util.Map(rows, func(a T) M {
		return *d.MapItem(a)
	})
}

func GetDefaultMapper[T any, M any](mappingFunction func(item T) *M) Mapper[T, M] {
	return &defaultMapperImpl[T, M]{
		mapItem: mappingFunction,
	}
}
