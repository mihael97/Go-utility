package swagger

import (
	"fmt"
	"github.com/swaggo/files"
	"log"
	"strings"
)

func InitSwagger(config SwaggerConfig) {
	if len(strings.TrimSpace(config.ApiPath)) == 0 {
		log.Panic("Api path is empty!")
	}
	config.Docs.BasePath = config.ApiPath
	if config.AppName != nil {
		config.Docs.Title = fmt.Sprintf("%s backend", strings.ToTitle(*config.AppName))
	}
	swaggerPath := "/api/swagger/*any"
	config.Router.Handle(swaggerPath, swaggerFiles.Handler)
}
