package currency

import util "gitlab.com/mihael97/Go-utility/internal/currency"

type Currency struct {
	Name        string  `json:"name"`
	BuyingRate  float64 `json:"buyingRate"`
	MiddleRate  float64 `json:"middleRate"`
	SellingRate float64 `json:"sellingRate"`
	Enabled     bool    `json:"enabled"`
}

func (currency *Currency) ConvertBuyingRate(amount float64, fromRate float64) float64 {
	return util.Convert(amount, fromRate, currency.BuyingRate)
}

func (currency *Currency) ConvertMiddleRate(amount float64, fromRate float64) float64 {
	return util.Convert(amount, fromRate, currency.MiddleRate)
}

func (currency *Currency) ConvertSellingRate(amount float64, fromRate float64) float64 {
	return util.Convert(amount, fromRate, currency.SellingRate)
}
