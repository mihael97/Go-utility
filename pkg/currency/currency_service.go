package currency

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	util "gitlab.com/mihael97/Go-utility/internal/currency"
	"net/http"
	"os"
	"sort"
	"time"
)

const URL = "https://api.hnb.hr/tecajn-eur/v3"
const CacheExpiration = 1 * time.Hour * 24
const CacheKey = "CURRENCY_RATES"
const HrkLabel = "HRK"
const EurLabel = "EUR"
const EuroHrkConversionRate = 7.53450

var EuroEnabled = time.Now().After(time.Date(2023, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC+1", 0)))

var currencyService *currencyServiceImpl

type currencyServiceImpl struct {
	client *redis.Client
}

func GetCurrencyService() CurrencyService {
	if currencyService == nil {
		currencyService = &currencyServiceImpl{}
		if len(os.Getenv("REDIS_URL")) != 0 && len(os.Getenv("REDIS_PASSWORD")) != 0 {
			currencyService.client = redis.NewClient(&redis.Options{
				Addr:     os.Getenv("REDIS_URL"),
				Password: os.Getenv("REDIS_PASSWORD"),
				DB:       0,
			})
		}
	}
	return currencyService
}

func ParseCurrencies(jsonCurrencies []map[string]interface{}) ([]Currency, error) {
	currencies := make([]Currency, len(jsonCurrencies))
	for index, x := range jsonCurrencies {
		buyingRate, err := util.PrepareValue(x, "kupovni_tecaj")
		if err != nil {
			return nil, err
		}
		middleRate, err := util.PrepareValue(x, "srednji_tecaj")
		if err != nil {
			return nil, err
		}
		sellingRate, err := util.PrepareValue(x, "prodajni_tecaj")
		if err != nil {
			return nil, err
		}

		currencies[index] = Currency{
			Name:        x["valuta"].(string),
			BuyingRate:  buyingRate,
			MiddleRate:  middleRate,
			SellingRate: sellingRate,
			Enabled:     true,
		}
	}
	return currencies, nil
}

func (service *currencyServiceImpl) GetRates() ([]Currency, error) {
	currencies := make([]Currency, 0)

	if service.client != nil {
		items, err := service.client.Get(context.Background(), CacheKey).Result()
		if err == nil {
			if err := json.Unmarshal([]byte(items), &currencies); err != nil {
				return nil, err
			}
			return currencies, nil
		} else if err != redis.Nil {
			return nil, err
		}
	}

	response, err := http.Get(URL)
	if err != nil {
		return nil, err
	}

	var jsonCurrencies []map[string]interface{}
	if err := json.NewDecoder(response.Body).Decode(&jsonCurrencies); err != nil {
		return nil, err
	}

	currencies, err = ParseCurrencies(jsonCurrencies)
	if err != nil {
		return nil, err
	}

	currencyForAppend := Currency{}

	if EuroEnabled {
		currencyForAppend.MiddleRate = 1
		currencyForAppend.SellingRate = 1
		currencyForAppend.BuyingRate = 1
		currencyForAppend.Name = EurLabel
		currencyForAppend.Enabled = true
	} else {
		currencyForAppend.MiddleRate = 1
		currencyForAppend.SellingRate = 1
		currencyForAppend.BuyingRate = 1
		currencyForAppend.Name = HrkLabel
	}
	currencyForAppend.Enabled = true
	currencies = append(currencies, currencyForAppend)
	sort.Slice(currencies, func(i, j int) bool {
		return currencies[i].Name == currencyForAppend.Name
	})
	if EuroEnabled {
		currencies = append(currencies, Currency{
			Name:        HrkLabel,
			BuyingRate:  EuroHrkConversionRate,
			MiddleRate:  EuroHrkConversionRate,
			SellingRate: EuroHrkConversionRate,
			Enabled:     false,
		})
	}

	if service.client != nil {
		service.client.Set(context.Background(), CacheKey, currencies, CacheExpiration)
	}

	return currencies, err
}

func (service *currencyServiceImpl) Convert(fromRate float64, toRate float64, amount float64) float64 {
	return amount * toRate / fromRate
}

func (service *currencyServiceImpl) GetByName(name string) (*Currency, error) {
	rates, err := service.GetRates()
	if err != nil {
		return nil, err
	}
	for _, rate := range rates {
		if rate.Name == name {
			return &rate, nil
		}
	}
	return nil, nil
}
