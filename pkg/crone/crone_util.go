package crone

import "gopkg.in/robfig/cron.v2"

func InitializeCroneSchedulers(statements map[string]func()) error {
	scheduler := cron.New()
	for cronSchedule, statement := range statements {
		if _, err := scheduler.AddFunc(cronSchedule, statement); err != nil {
			return err
		}
	}
	scheduler.Start()
	return nil
}
