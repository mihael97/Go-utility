package util

type Exception struct {
	Type string `json:"type"`
}

type GenericException struct {
	*Exception
	Message string `json:"message"`
}

type ValidationException struct {
	*Exception
	Errors map[string][]string `json:"errors"`
}

func CreateValidationException(errors map[string][]string) ValidationException {
	return ValidationException{
		Exception: &Exception{Type: "validation"},
		Errors:    errors,
	}
}

func NewException(err string) GenericException {
	return GenericException{
		Exception: &Exception{Type: "generic"},
		Message:   err,
	}
}
