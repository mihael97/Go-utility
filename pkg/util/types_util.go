package util

func GetPointer[T any](value T) *T {
	pointerValue := value
	return &pointerValue
}

func GetValueOrDefault[T any](value *T, defaultValue T) T {
	if value == nil {
		return *value
	}
	return defaultValue
}
