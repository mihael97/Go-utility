package v1

import (
	log "github.com/sirupsen/logrus"
	util "gitlab.com/mihael97/Go-utility/internal/database"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var database *gorm.DB = nil

func InitializeDatabase(tables []interface{}, dbConfig ...util.GormConfig) {
	var err error
	_, connectionInfo, err := util.GetDatabaseConnectionString()
	if err != nil {
		log.Panic(err)
	}

	gormConfig := util.MapGormConfig(dbConfig)

	database, err = gorm.Open(postgres.Open(connectionInfo), &gormConfig)
	if err != nil {
		log.Panic(err)
	}

	for _, table := range tables {
		err = database.AutoMigrate(&table)
		if err != nil {
			log.Panic(err)
		}
	}
}

func InitializeModel(tables ...interface{}) {
	err := database.AutoMigrate(tables)
	if err != nil {
		log.Panic(err)
	}
}

func GetORMDatabase() *gorm.DB {
	if database == nil {
		return nil
	}
	return database
}
