package v1

import (
	"database/sql"
	_ "github.com/jackc/pgx/v4/stdlib"
	log "github.com/sirupsen/logrus"
	util "gitlab.com/mihael97/Go-utility/internal/database"
)

var db *sql.DB

func GetDatabase() *sql.DB {
	if db == nil {
		var err error
		db, err = util.OpenConnection()
		if err != nil {
			log.Panicln(err)
		}
	}
	return db
}
