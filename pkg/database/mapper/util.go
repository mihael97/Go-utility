package mapper

import (
	"database/sql"
)

func MapRows(rows *sql.Rows, first ...bool) (SqlRowsData, error) {
	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}
	result := make([]map[string]*interface{}, 0)
	sqlRowResult := make([]interface{}, len(cols))
	for i := range sqlRowResult {
		sqlRowResult[i] = new(interface{})
	}
	for rows.Next() {
		if err := rows.Scan(sqlRowResult[:]...); err != nil {
			return nil, err
		}
		cur := make(map[string]*interface{}, len(cols))
		for i := range sqlRowResult {
			val := *sqlRowResult[i].(*interface{})
			var dbValue *interface{}
			if val == nil {
				dbValue = nil
			} else {
				dbValue = &val
			}
			cur[cols[i]] = dbValue
		}

		result = append(result, cur)

		if len(first) != 0 && first[0] {
			break
		}
	}
	return CreateSqlRowsData(result), nil
}
