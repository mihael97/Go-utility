package mapper

import (
	"database/sql"
	"gitlab.com/mihael97/Go-utility/pkg/util"
)

type databaseMapper[T any] struct {
	mappingFunction func(rows SqlRowsData) T
}

func (d databaseMapper[T]) MapItem(rows *sql.Rows) (*T, error) {
	mappedRows, err := MapRows(rows, true)
	if err != nil {
		return nil, err
	} else if mappedRows.IsEmpty() {
		return nil, nil
	}
	return util.GetPointer(d.mappingFunction(mappedRows)), nil
}

func (d databaseMapper[T]) MapItems(rows *sql.Rows) ([]T, error) {
	mappedRows, err := MapRows(rows)
	if err != nil {
		return nil, err
	}

	result := make([]T, 0)

	for mappedRows.HasNext() {
		result = append(result, d.mappingFunction(mappedRows))
	}

	return result, nil
}

func GetDatabaseMapper[T any](mappingFunction func(rows SqlRowsData) T) DatabaseMapper[T] {
	return databaseMapper[T]{mappingFunction: mappingFunction}
}
