package mapper

import "database/sql"

type DatabaseMapper[T any] interface {
	MapItem(rows *sql.Rows) (*T, error)
	MapItems(rows *sql.Rows) ([]T, error)
}

type SqlRowsData interface {
	Size() int
	IsEmpty() bool
	HasValue(field string) bool
	Contains(field string) bool
	GetBool(field string) bool
	GetInt64(field string) int64
	GetInt(field string) int
	GetFloat64(field string) float64
	GetData(field string) *interface{}
	GetString(field string) string
	HasNext() bool
	RowAtIndex(index int) map[string]*interface{}
}
