package dao

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

type db[T comparable] struct {
	db *sql.DB
	tx *sql.Tx
}

func (d *db[T]) StopTransaction() error {
	defer func() { d.tx = nil }()
	return d.tx.Rollback()
}

func (d *db[T]) GetTransaction() (*sql.Tx, error) {
	if d.tx == nil {
		err := d.StartTransaction()
		if err != nil {
			return nil, err
		}
	}
	return d.tx, nil
}

func (d *db[T]) ExecuteNamed(s string, t T) error {
	args, err := getArgs(t)
	if err != nil {
		return err
	}
	return d.Execute(s, args...)
}
func (d *db[T]) CreateNamed(s string, t T) (*int64, error) {
	args, err := getArgs(t)
	if err != nil {
		return nil, err
	}
	return d.Create(s, args)
}

func (d *db[T]) QueryForObject(query string, mapping func(row *sql.Row) (*T, error), args ...any) (*T, error) {
	row := d.db.QueryRow(query, args...)
	item, err := mapping(row)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	} else if item == nil {
		return nil, fmt.Errorf("expected one row")
	}

	return item, nil
}

func (d *db[T]) Execute(s string, a ...any) (err error) {
	toCommit := false

	if d.tx == nil {
		toCommit = true
		d.tx, err = d.db.Begin()
		if err != nil {
			return
		}
	}

	result, err := d.db.Exec(s, a...)
	if err != nil {
		return err
	} else if rowsAffected, _ := result.RowsAffected(); rowsAffected == 0 {
		return fmt.Errorf("no row deleted")
	}

	if toCommit {
		err = d.tx.Commit()
	}

	return
}

func (d *db[T]) GetRows(s string, a ...any) (*sql.Rows, error) {
	return d.db.Query(s, a...)
}

func (d *db[T]) CommitTransaction() error {
	if d.tx == nil {
		return errors.New("transaction is not active")
	}
	defer func() { d.tx = nil }()
	return d.tx.Commit()
}

func (d *db[T]) StartTransaction() (err error) {
	d.tx, err = d.db.Begin()
	return
}

func (d *db[T]) QueryForList(s string, f func(row *sql.Rows) (*T, error), a ...any) (result []T, err error) {
	var rows *sql.Rows
	rows, err = d.db.Query(s, a...)
	if err != nil {
		return nil, err
	}

	result = make([]T, 0)

	for rows.Next() {
		item, err := f(rows)
		if err != nil {
			return nil, err
		}
		result = append(result, *item)
	}

	return
}

func (d *db[T]) Create(query string, args ...any) (*int64, error) {
	toCommit := false

	if d.tx == nil {
		toCommit = true
		var err error
		d.tx, err = d.db.Begin()
		if err != nil {
			return nil, err
		}
	}

	rows, err := d.db.Exec(query, args...)
	if err != nil {
		return nil, err
	}
	if insertedRows, _ := rows.RowsAffected(); insertedRows == 0 {
		return nil, errors.New("id not returned")
	}
	var id int64
	id, err = rows.LastInsertId()

	if toCommit {
		err = d.tx.Commit()
		if err != nil {
			return nil, err
		}
		d.tx = nil
	}

	return &id, nil
}

func GetDatabase[T comparable](dbItem *sql.DB) DB[T] {
	var dbInstance *sql.DB

	if dbItem == nil {
		dbInstance = openConnection()
		err := dbInstance.Ping()
		if err != nil {
			logrus.Panic(err)
		}
	} else {
		dbInstance = dbItem
	}

	return &db[T]{
		dbInstance,
		nil,
	}
}
