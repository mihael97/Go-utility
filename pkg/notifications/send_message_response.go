package dto

type SendMessageResponse struct {
	SendMessageCommon
	MessageId string `json:"messageId"`
	Status    string `json:"status"`
}
