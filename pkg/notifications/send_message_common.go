package dto

type SendMessageCommon struct {
	Type    string  `json:"type,omitempty"`
	Message *string `json:"message,omitempty"`
}
