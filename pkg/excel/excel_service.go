package excel

import (
	"bytes"
	"github.com/xuri/excelize/v2"
	"log"
	"strings"
)

func CreateDocument(name string) *Document {
	return &Document{Name: name, file: excelize.NewFile()}
}

func (document *Document) CreateSheet(sheet string) {
	document.file.NewSheet(sheet)
}

func (document *Document) Write(position string, content string, sheet ...string) {
	if len(sheet) == 0 {
		sheet = []string{"Sheet1"}
	}
	if err := document.file.SetCellValue(sheet[0], position, content); err != nil {
		log.Fatal(err)
	}
}

func (document *Document) Read(sheet string, position string) string {
	cell, err := document.file.GetCellValue(sheet, position)
	if err != nil {
		log.Fatal(cell)
	}
	return cell
}

func prepareName(name string) string {
	if strings.HasSuffix(name, ".xlsx") {
		return name
	}
	return name + ".xlsx"
}

func (document *Document) Save() {
	if err := document.file.SaveAs(prepareName(document.Name)); err != nil {
		log.Fatal(err)
	}
}

func (document *Document) GetBytes() (*bytes.Buffer, error) {
	return document.file.WriteToBuffer()
}
