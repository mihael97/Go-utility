package excel

import "github.com/xuri/excelize/v2"

type Document struct {
	Name string
	file *excelize.File
}
