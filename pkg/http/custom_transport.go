package http

import (
	"log"
	"net/http"
)

type CustomTransport struct {
	rt      http.RoundTripper
	headers map[string]string
}

func CreateCustomHeader(rt http.RoundTripper, headers map[string]string) http.RoundTripper {
	if rt == nil {
		log.Println("Round tripper is nil. Using default")
		rt = http.DefaultTransport
	}
	return CustomTransport{
		rt:      rt,
		headers: headers,
	}
}

func (c CustomTransport) RoundTrip(request *http.Request) (*http.Response, error) {
	for key, value := range c.headers {
		request.Header.Set(key, value)
	}
	return c.rt.RoundTrip(request)
}
