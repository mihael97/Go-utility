package email

import (
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	dto "gitlab.com/mihael97/Go-utility/pkg/notifications"
	"golang.org/x/net/context"
	"os"
)

func SendEmail(command EmailRequest) error {
	redisUrl, exists := os.LookupEnv("REDIS_URL")
	if !exists {
		profile := os.Getenv("PROFILE")
		if profile == "prod" {
			logrus.Panic("Redis url not provided")
		} else {
			redisUrl = "localhost:6379"
		}
	}
	redisPassword, exists := os.LookupEnv("REDIS_PASSWORD")
	if !exists {
		redisPassword = ""
	}
	rdb := redis.NewClient(&redis.Options{
		Addr:     redisUrl,
		Password: redisPassword,
		DB:       0, // use default DB
	})
	defer rdb.Close()

	data := map[string]string{
		"to":      command.To,
		"body":    command.Body,
		"subject": command.Subject,
	}

	if command.From != nil {
		data["from"] = *command.From
	}

	notificationCommand := dto.SendMessageCommand{
		SendMessageCommon: dto.SendMessageCommon{
			Type:    "request",
			Message: nil,
		},
		MessageId: "",
		Client:    "",
		Data:      data,
		Channel:   "email",
	}

	requestBody, _ := json.Marshal(&notificationCommand)

	response := rdb.Publish(context.Background(), "notification", requestBody)
	return response.Err()
}
