package util

import (
	"strconv"
	"time"
)

type MillisTime time.Time

func FromDate(date time.Time) *MillisTime {
	millisDate := MillisTime(date)
	return &millisDate
}

func (m *MillisTime) UnmarshalJSON(content []byte) error {
	millisTime, err := strconv.ParseInt(string(content), 10, 64)
	if err != nil {
		return nil
	}
	*m = MillisTime(time.UnixMilli(millisTime))
	return nil
}

func (m *MillisTime) MarshalJSON() ([]byte, error) {
	date := time.Time(*m)
	millis := date.UnixMilli()
	return []byte(strconv.FormatInt(millis, 10)), nil
}

func (m *MillisTime) ToTime() time.Time {
	return time.Time(*m)
}

func (m *MillisTime) Compare(date *MillisTime) int64 {
	if date == nil {
		return 1
	}
	return m.ToTime().UnixMilli() - date.ToTime().UnixMilli()
}
