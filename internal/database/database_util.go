package database

import (
	"database/sql"
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"log"
	"time"
)

func OpenConnection() (*sql.DB, error) {
	dbType, connectionInfo, err := GetDatabaseConnectionString()
	if err != nil {
		log.Panicln(err)
	}
	dbInstance, err := sql.Open(dbType, connectionInfo)
	if len(env.GetEnvVariable("SKIP_MAX_LIFETIME", "")) == 0 {
		dbInstance.SetMaxIdleConns(50)
		dbInstance.SetMaxOpenConns(50)
		dbInstance.SetConnMaxLifetime(5 * time.Second)
	}
	return dbInstance, err
}
