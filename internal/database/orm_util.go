package database

import (
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"log"
	"strings"
)

func MapGormConfig(config []GormConfig) gorm.Config {
	if len(config) == 0 {
		return gorm.Config{}
	} else if len(config) != 1 {
		log.Panic("Invalid config size", len(config))
	}
	schemaName := config[0].SchemaName

	if len(schemaName) == 0 {
		schemaName = "public"
	} else if !strings.HasSuffix(schemaName, ".") {
		schemaName = schemaName + "."
	}

	return gorm.Config{NamingStrategy: schema.NamingStrategy{
		TablePrefix:   schemaName, // schema name
		SingularTable: false,
	}}
}
