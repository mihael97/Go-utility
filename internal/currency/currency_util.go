package currency

import (
	"strconv"
	"strings"
)

func Convert(amount float64, fromRate float64, toRate float64) float64 {
	return amount / fromRate * toRate
}

func PrepareValue(value map[string]interface{}, key string) (float64, error) {
	x, err := strconv.ParseFloat(strings.ReplaceAll(value[key].(string), ",", "."), 64)
	return x, err
}
